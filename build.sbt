
name := "ASD Project 2"
 
version := "1.0"
 
scalaVersion := "2.11.8"
 
libraryDependencies +=
  "com.typesafe.akka" %% "akka-actor" % "2.4.10"

libraryDependencies +=
	"com.typesafe.akka" %% "akka-remote" % "2.4.10"
libraryDependencies += "com.typesafe" % "config" % "1.2.1"  

resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"
resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"