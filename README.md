README


Usefull links:

Scalla actors intro:
	https://doc.akka.io/docs/akka/current/scala/actors.html

Scalla remote comunication:
	https://doc.akka.io/docs/akka/2.5/scala/remoting.html

Runtime configs:
	https://groups.google.com/forum/#!topic/akka-user/eoyfB5apWvQ


Scalla multi JVM Testing
	https://doc.akka.io/docs/akka/2.5/scala/multi-jvm-testing.html

Scala tutorial, wierd time stuff:
	https://www.scala-lang.org/docu/files/ScalaTutorial.pdf

Scala List Handeling
	https://www.tutorialspoint.com/scala/scala_lists.htm


Programmatical *.conf definitions
	https://danielasfregola.com/2015/06/01/loading-configurations-in-scala/
