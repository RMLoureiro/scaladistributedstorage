#!/bin/bash

myHostname="10.22.111.37"   #this must be defined in aplication.conf
myPort=5150

contactHostname="10.22.106.248"
contactPort=5150


echo "Starting $1 processes"

gnome-terminal -e "java -Dakka.remote.netty.tcp.port=$myPort -jar target/scala-2.11/ASD\ Project\ 2-assembly-1.0.jar $contactHostname:$myPort"
sleep 1


contactPort=$myPort
myPort=`expr $myPort + 10`



for ((i = 2; i <= $1; i++));
do

	gnome-terminal -e "java -Dakka.remote.netty.tcp.port=$myPort -jar target/scala-2.11/ASD\ Project\ 2-assembly-1.0.jar $myHostname:$contactPort"
	sleep 1
	
	contactPort=$myPort
	myPort=`expr $myPort + 10`

done

echo "done"
