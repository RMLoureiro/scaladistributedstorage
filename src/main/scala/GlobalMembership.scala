import akka.actor._
import akka.util.Timeout

import scala.concurrent.duration._
import scala.language.postfixOps
import akka.actor.Cancellable
import scala.collection.mutable.Set

import scala.concurrent.ExecutionContext.Implicits.global

class GlobalMembership(myHostname : String ,myPort : String) extends Actor{

	
	val myID   = myHostname + ":" + myPort
	
	val hyparID = "akka.tcp://ASD@"+myID+"/user/hyparview"
	val globalID = "akka.tcp://ASD@"+myID+"/user/globalmembership"
	val replicationID = "akka.tcp://ASD@"+myID+"/user/replication"
	val storageID = "akka.tcp://ASD@"+myID+"/user/storage"
  	
	var tombstone : Set[String] = Set()
	val r = new scala.util.Random
	var id = r.nextInt(1000)
	var globalView: Set [String] = Set(myID)
	var globalMap : Map[Int, String] = Map()

	def receive = {

		case StartGV(contact) =>
			println("StartGV")
			println(getRefFromID(contact))
			getRefFromID(contact) ! AskGV


		case AskGV =>
			println("AskGV")
			sender ! CurrentGV(globalView)

		case CurrentGV(contactGlobalView) =>
			print("CurrentGV, ")
			println("Sender =  " + sender)
			globalView = contactGlobalView + myID

		case PrintGV =>
			println("GlobalMembership " + globalView.size)
			for (actor <- globalView)
				println(actor)
			println("GlobalMap " + globalMap.size)
			for (actor <- globalMap)
				println(actor._1)
			println()

	    case DeadDude(actor) =>
	    	//println("GlobalMembership Recieved DeadDude")
				globalView -= actor
				var id = -1
				globalMap.foreach(actor =>
					if(actor._2.equals(actor)){
						id = actor._1
					}
				)
				globalMap -= id
				tombstone += actor
				println(" tombstone = " + tombstone)
				context.actorSelection(hyparID) ! Obituary(actor)
				context.actorSelection(replicationID) ! DeadDude(actor)


	    case NewDude(actor, id) =>
	    	//println("GlobalMembership Recieved NewDude")
			if(!tombstone.contains(actor)) {
				globalView += actor
				globalMap += id->actor
				context.actorSelection(replicationID) ! NewDude(actor, id)
			}

		case GetNewId(newId) =>
			val r = new scala.util.Random
			var id : Int = 0
			do{
				id = r.nextInt(1000)
			}while(globalMap.contains(id))
			getRefFromID(newId) ! GetId(id)
			sender ! ContinueJoin(newId, id)


			case GetId(id) =>
				this.id = id

  }

	def getRefFromID(ID:String) : ActorSelection = {
		context.actorSelection("akka.tcp://ASD@"+ID+"/user/globalmembership")
	}


}

	