import scala.collection.mutable
import akka.actor._
import scala.concurrent.duration._


//val s = sender    // Actor[akka://<system>@<host>:<port>/user/path/to/actor]
//val p = s.path    // akka://<system>@<host>:<port>/user/path/to/actor
//val a = p.address // akka://<system>@<host>:<port>
//val host = a.host // Some(<host>), where <host> is the listen address as configured for the remote system
//val port = a.port // Some(<port>), where <port> is the actual listen port of the remote system
//Gossip algoritmo 1 aula teorica 2


class Gossip(myHostname : String ,myPort : String) extends Actor {

  val fanout : Int = 4
  var delivered : mutable.HashMap[String, ((String,String, Int), String)] = mutable.HashMap()
  var pending :   mutable.HashMap[String, ((String,String, Int), String)] = mutable.HashMap()
  var neighbors : List[String] = List()
  var counter : Int = 0


  val myID   = myHostname + ":" + myPort

  val hyparID  = "akka.tcp://ASD@"+myID+"/user/hyparview"
  val globalID = "akka.tcp://ASD@"+myID+"/user/globalmembership"
  val replicationID = "akka.tcp://ASD@"+myID+"/user/replication"

  def receive = {

    case RBroadcast(message) =>
      //println("Gossip received RBroadcast " + message)
      val mid = self.toString() + counter.toString
      message._1 match {
        case "NewDude" =>context.actorSelection(globalID) ! NewDude(message._2, message._3)
        case "DeadDude" =>context.actorSelection(globalID) ! DeadDude(message._2)
        case "StartUp" => context.actorSelection(replicationID) ! StartUp
      }
      delivered += mid -> (message,myID)
      pending += mid -> (message, myID)
      context.actorSelection(hyparID) ! ManosAtiva
      counter += 1

    case CurrentNeighbors(actors) =>
      //println("Gossip received CurrentNeighbors " + actors)
      neighbors = actors
      pending.foreach { 
        case (mid, (message, origin)) =>
        val targets = scala.util.Random.shuffle(neighbors).take(fanout)
        targets.foreach { target =>
          if(!origin.equals(target)) {
            getRefFromID(target) ! GossipMessage(mid, message, origin)
          }
        }
      }
      pending.empty

    case GossipMessage(mid, message, origin) =>
      if(!delivered.contains(mid)){
        //println("Gossip received GossipMessage " + message)
        delivered += mid -> (message, origin)
        message._1 match {
          case "NewDude" =>context.actorSelection(globalID) ! NewDude(message._2, message._3)
          case "DeadDude" =>context.actorSelection(globalID) ! DeadDude(message._2)
        }

        pending += mid -> (message, origin)
        context.actorSelection(hyparID) ! ManosAtiva
      }

    case AntiEntropyTrigger =>
      //println("Gossip received AntiEntropyTrigger")
      if (neighbors.nonEmpty) {
        //println("Gossip received AntiEntropyTrigger")
        neighbors = scala.util.Random.shuffle(neighbors)
        val process = getRefFromID(neighbors.head)
        val knowMessages: List[String] = delivered.keySet.toList
        process ! AntiEntropy(knowMessages)
      }else{
        context.actorSelection(hyparID) ! ManosAtiva
      }

    case AntiEntropy(messages) =>
      //println("Gossip received AntiEntropy")
      delivered.foreach{ del =>
        if (!messages.contains(del._1)){
          sender ! GossipMessage(del._1, del._2._1, del._2._2)
        }
      }

    case a =>
      println("WAO*ISJF*AUHNFIEUPHFªEW*UHNF")
      println(a)
  }


    def getRefFromID(ID:String) : ActorSelection = {
      context.actorSelection("akka.tcp://ASD@"+ID+"/user/gossip")
    }
  
}