import akka.actor.ActorSystem
import akka.actor.ActorRef
import akka.actor.Actor
import akka.actor.Props
import akka.remote._
import scala.concurrent.duration._
import com.typesafe.config.ConfigFactory
import java.io.File
import java.net.ServerSocket
import java.util.concurrent.RejectedExecutionException
import scala.concurrent.duration.Duration;
import java.util.concurrent.TimeUnit;
import scala.util.Random


// assign a port akka remote  -Dakka.remote.netty.port=xxxx
// XXXX will be that node's ID

// use val mamboRemote = context.actorSelection("akka.tcp://ASD@127.0.0.1:NODEID/user/PROTOCOL")
// to comunicate with protocols from other processes

object Main {

	def main(args: Array[String]) {


		if (args.length < 1) {
    	    println("dude, i need 1 parameter")
    	    println("i mean, get you shit together")
    	    println("and put it all in a backpack or something, you know?")
    	    println("so its together. Jesus Summer")
    	    throw new IllegalArgumentException("Exactly 1 parameter required !")
    	}

    	

    	val config =  ConfigFactory.load("aplication.conf")
		val system = ActorSystem("ASD",config)

		
		//context.system.settings.config.getString("akka.remote.netty.port")
		

		val myHostname 	= system.settings.config.getString("akka.remote.netty.tcp.hostname")
		val myPort 		= system.settings.config.getString("akka.remote.netty.tcp.port")
		val myID 		= myHostname+":"+myPort	

		val global 		= system.actorOf(Props(new GlobalMembership(myHostname,myPort)), name="globalmembership")
		val partial 	= system.actorOf(Props(new Hyparview(myHostname,myPort)) ,name="hyparview")
		val gossip 		= system.actorOf(Props(new Gossip(myHostname,myPort)), name="gossip")

		println("HELOOOOOO")

		//
		//	must change this later
		//
		val contact = args(0)

		println("My ID is = " + myID)
		println("Contact  = " + contact)

		if(!contact.equals(myID)){
			global ! StartGV(contact)			//you have a contact
			partial ! StartJoin(contact)
			
		}else{								//you are your own contact
			val r = new scala.util.Random
			var id = r.nextInt(1000)
			gossip ! RBroadcast(("NewDude",myID, id))
		}



		//Hyparview timers

		val random = new Random
		
		import system.dispatcher
		val activeViewPrintTimer =
			system.scheduler.schedule(
    		Duration.create(0, TimeUnit.MILLISECONDS),
		    Duration.create(10000, TimeUnit.MILLISECONDS),
    		partial,
    		PrintActiveView)

		val shuffleTimer =
			system.scheduler.schedule(
    		Duration.create(random.nextInt(10000)+ 10000, TimeUnit.MILLISECONDS),	//10 + x secs V x <10  
		    Duration.create(10000, TimeUnit.MILLISECONDS),	//10 secs
    		partial,
    		SuffleTrigger)

		val heartBeatTimer =
			system.scheduler.schedule(
    		Duration.create(10000, TimeUnit.MILLISECONDS),	//10 secs
		    Duration.create(5000, TimeUnit.MILLISECONDS),	//5 secs
    		partial,
    		SendHeartBeat)

		val antiEntropyTimer =
			system.scheduler.schedule(
			Duration.create(5000, TimeUnit.MILLISECONDS),	//10 secs
			Duration.create(1000, TimeUnit.MILLISECONDS),	//5 secs
			gossip,
			AntiEntropyTrigger)

		//activeViewPrintTimer.cancel()
		//shuffleTimer.cancel()
		//heartBeatTimer.cancel()
		//antiEntropyTimer.cancel()

		//TODO: Build command line interface

		var input = scala.io.StdIn.readLine()
		while (input != "exit"){
			
			input match {
				
				case "gv" => 
					global ! PrintGV

				case _ => 
					println("stop tickling me")
			}
			
			input = scala.io.StdIn.readLine()
		}

		
		try {
			system.terminate()
		} catch {
			case e: RejectedExecutionException => 
				println("Weird exception was thrown")
				println(e)
		}finally{
			println("BYE BYE BYE")
		}
	}


	
}