import akka.actor.Actor
import scala.collection.mutable.ListBuffer

class Replication(location : String) extends Actor{


  val myPort   	= context.system.settings.config.getString("akka.remote.netty.tcp.port")
  var myID 		= "akka.tcp://ASD@"+location+"/user/replication"
  val storage 	= "akka.tcp://ASD@"+location+"/user/storage"
  var globalMap : Map[Int,String] = Map()
  var idList 	: ListBuffer[Int] = ListBuffer()


  def receive ={

    case NewDude(actor, id)=>
      var myId = -1
      globalMap.foreach(ac =>
        if(ac._2.equals(myPort)){
          myId = ac._1
        }
      )

      globalMap += id -> actor
      idList += id
      idList = idList.sorted

      val op = distance(idList, myId, id)

      if(op >0){
        context.actorSelection(storage) ! NewStorage(op,actor, id, idList, globalMap)
      }

    case DeadDude(actor) =>
      var id = -1
      var myId = -1
      globalMap.foreach(ac =>
        if(ac._2.equals(actor)){
          id = ac._1
        }else if(ac._2.equals(myPort)){
          myId = ac._1
        }
      )
      globalMap -= id
      idList -= id
      val op = distance(idList, myId, id)

      if(op >0){
        context.actorSelection(storage) ! DeadStorage(op,actor,id,idList, globalMap)
      }else{
        context.actorSelection(storage) ! UpdateViews(idList, globalMap)
      }

    case StartUp =>
      context.actorSelection(storage) ! Initialize(globalMap, idList)

  }
  def distance(list : ListBuffer[Int], myId : Int, newId : Int) : Int ={
    var ind = list.indexOf(myId)
    val arr = list.toArray[Int]
    ind -= 1
    if(ind == -1){
      ind = arr.size
    }
    if(arr(ind) == newId){
      return 1
    }else{
      ind -= 1
      if(ind == -1){
        ind = arr.size
      }
      if(arr(ind) == newId) {
        return 2
      }
    }
    return -1
  }
}
