import akka.actor.{Cancellable, _}
import akka.util.Timeout

import scala.concurrent.duration._
import scala.concurrent.ExecutionContext.Implicits.global


class MPaxos(location : String) extends Actor{

  var serial = 0
  var myID = "akka.tcp://ASD@"+location+"/user/mpaxos"
  var leader = myID
  val storage = "akka.tcp://ASD@"+location+"/user/storage"
  var promisses : Map[(String, String, Int, String, Int, String), Cancellable] = Map()
  var pending : Set[(String, String, Int, String, Int, String)] = Set()
  var members : Set[ActorRef] = Set()

  def receive ={
    case Op(op) =>
      if(leader.equals(myID)){
        members.foreach( ref =>
          ref ! Propose(serial + 1, op, myID)

        )
        pending += op
      }else{
        val system = context.system
        val timeout = Timeout(7 seconds)
        promisses += op -> system.scheduler.scheduleOnce(timeout.duration, self, TakeOver(op))
      }

    case TakeOver(op)=>
      members.foreach( ref =>
        ref ! Propose(serial + 1, op, myID)

      )
      pending += op


    case Propose(serial, op, ref) =>
      if(this.serial < serial){
        this.serial = serial
        leader = ref
        sender ! Accept(serial, op, ref)
        context.actorSelection(op._6) ! OrderedOp(serial, op)
      }

    case Accept(serial, op, ref)=>
      if(promisses.contains(op)){
        promisses -= op
        this.serial = serial
        leader = ref
        context.actorSelection(op._6) ! OrderedOp(serial, op)
      }else{

      }


    case NewMember(actor) =>
      members += actor

    case DeadMember(actor) =>
      members -= actor

    case MPaxosStartup(ref) =>
      ref ! MPaxosConstructor(leader,serial)

    case MPaxosConstructor(leader,serial)=>
      this.leader = leader
      this.serial = serial
  }

}