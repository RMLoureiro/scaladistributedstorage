import akka.actor.{Actor, ActorRef, PoisonPill, Props}

import scala.collection.mutable.ListBuffer

class Storage(location : String) extends Actor{

  var myID = "akka.tcp://ASD@"+location+"/user/storage"
  var lastOp = -1
  var myBucket : Int = -1
  var stopped = false
  var started = false
  var replicationSet : Map[Int, ActorRef] = Map()
  var opQueue: Map[Int, ListBuffer[(Int, (String, String, Int , String, Int, String))]] = Map()
  var buckets : Map[Int, Map[Int, String]] = Map()
  var globalMap : Map[Int,String] = Map()
  var idList 	: ListBuffer[Int] = ListBuffer()

  def receive={
    case Initialize(globalMap,  idList)=>
      started = true
      this.globalMap = globalMap
      this.idList = idList
      globalMap.foreach( actor =>
        if(actor._2.equals(myID)){
          myBucket = actor._1
        }
      )

      val pax = context.system.actorOf(Props(new MPaxos(location)), name="mpaxos" + myBucket)
      replicationSet += myBucket -> pax

      val arr = idList.toArray
      var index = arr.indexOf(myID)

      buckets += myBucket -> Map()

      index += 1
      if(index == arr.size){
        index = 0
      }
      var ac = globalMap.get(arr(index))
      context.actorSelection(ac.head) ! AddToReplication(myBucket, pax, -1)


      index += 1
      if(index == arr.size){
        index = 0
      }

      ac = globalMap.get(arr(index))
      context.actorSelection(ac.head) ! AddToReplication(myBucket, pax, -1)


    case AddToReplication(newBucket, pax, oldBucket) =>
      val myPax = context.system.actorOf(Props(new MPaxos(location)), name="mpaxos" + myBucket)
      replicationSet += myBucket -> myPax
      myPax ! NewMember(pax)
      sender ! AddPax(newBucket, myPax)
      buckets += newBucket -> Map()
      if(oldBucket >= 0){
        replicationSet.get(oldBucket).head ! PoisonPill
        replicationSet -= oldBucket
        buckets -= oldBucket
      }
      sender ! AddRepAck

    case AddPax(bucket, pax) =>
      replicationSet.get(bucket).head ! NewMember(pax)


    case Read(clientID, id) =>
      if(id < idList(0)){
        if(myID == idList(idList.size - 1)){
          sender ! ReadAck(id, buckets.get(myBucket).head.get(id).head)
        }else{
          context.actorSelection(globalMap.get(idList(idList.size - 1)).head) ! ReRead(myID, clientID, id)
        }
      }else{
        val it = idList.iterator
        var next = -1
        while(it.hasNext){
          next = it.next()
          if(next <= id){
            if(next == myBucket){
              if(!buckets.get(myBucket).head.contains(id)){
                var buc = buckets.get(myBucket).head
                buckets -= myBucket
                buc += id -> ""
                buckets += myBucket -> buc
              }
              sender ! ReadAck(id, buckets.get(myBucket).head.get(id).head)
            }else {
              context.actorSelection(globalMap.get(next).head) ! ReRead(myID, clientID, id)
            }
          }
        }
      }


    case ReRead(buttersID, clientID, id) =>
      if(!buckets.contains(myBucket)){
        var buc = buckets.get(myBucket).head
        buckets -= myBucket
        buc += id -> ""
        buckets += myBucket -> buc
      }

      context.actorSelection(buttersID) ! ReadResponse( clientID, id, buckets.get(myBucket).head.get(id).head)

    case ReadResponse(clientID, id, content) =>
      context.actorSelection(clientID) ! ReadAck(id, content)


    case Write(clientID, id, content)=>
      if(id < idList(0)){
        if(myID == idList(idList.size - 1)){
          replicationSet.get(myBucket).head ! Op("write", clientID, id, content, myBucket, myID)
        }else{
          context.actorSelection(globalMap.get(idList(idList.size - 1)).head) ! ReWrite(myID, clientID, id, content, idList(idList.size - 1))
          context.actorSelection(globalMap.get(idList(0)).head) ! ReWrite(myID, clientID, id, content, idList(0))
          context.actorSelection(globalMap.get(idList(1)).head) ! ReWrite(myID, clientID, id, content, idList(1))
        }
      }else{
        val it = idList.iterator
        var next = -1
        while(it.hasNext){
          next = it.next()
          if(next <= id){
            if(next == myBucket){
             replicationSet.get(myBucket).head ! Op("write", clientID, id, content, myBucket, myID)
            }else {
              context.actorSelection(globalMap.get(next).head) ! ReWrite(myID, clientID, id, content, next)
              if(it.hasNext){
                next = it.next()
                context.actorSelection(globalMap.get(next).head) ! ReWrite(myID, clientID, id, content, next)
                if(it.hasNext){
                  next = it.next()
                  context.actorSelection(globalMap.get(next).head) ! ReWrite(myID, clientID, id, content, next)
                }else{
                  context.actorSelection(globalMap.get(idList(0)).head) ! ReWrite(myID, clientID, id, content, idList(0))
                }
              }else{
                context.actorSelection(globalMap.get(idList(0)).head) ! ReWrite(myID, clientID, id, content, idList(0))
                context.actorSelection(globalMap.get(idList(1)).head) ! ReWrite(myID, clientID, id, content, idList(1))
              }
            }
          }
        }
      }


    case ReWrite(buttersID, clientID, id, content, bucket)=>
      replicationSet.get(bucket).head ! Op("write", clientID, id, content, bucket, myID)



    case OrderedOp(serial, op) =>
      var bucket = op._5
      if(!stopped && opQueue.get(bucket).head.size == 0 && lastOp == serial -1){
        var buc = buckets.get(op._5).head
        buckets -= op._5
        buc += op._3 -> op._4
        buckets += op._5 -> buc
        context.actorSelection(op._2) ! AckWrite(op._3, op._4)
        lastOp += 1
      }else {
        var ops = opQueue.get(bucket).head
        var newList: ListBuffer[(Int, (String, String, Int, String, Int, String))] = ListBuffer()
        opQueue -= bucket
        var c = 0
        while (ops(c)._1 < serial && c < ops.size) {
          newList += ops(c)
          c += 1
        }
        val member = (serial, op)
        newList += member
        while (c < ops.size) {
          newList += ops(c)
          c += 1
        }
        c = 0
        if(!stopped) {
          while (lastOp == newList(c)._1 - 1 && c < newList.size) {
            if (newList(c)._2._1.equals("write")) {
              var buc = buckets.get(newList(c)._2._5).head
              buckets -= newList(c)._2._5
              buc += newList(c)._2._3 -> op._4
              buckets += newList(c)._2._5 -> buc
              context.actorSelection(newList(c)._2._2) ! AckWrite(newList(c)._2._3, newList(c)._2._4)
              lastOp += 1
            } else if (newList(c)._2._1.equals("new")) {
               NewDude(newList(c)._2._3,newList(c)._2._2,newList(c)._2._5)
            } else {
              DeadDude(newList(c)._2._3,newList(c)._2._2,newList(c)._2._5)
            }
          }
          var newNewList :ListBuffer[(Int, (String, String, Int, String, Int, String))] = ListBuffer()
          while(c < newList.size){
            newNewList += newList(c)
            c += 1
          }
          opQueue += bucket -> newNewList
        }
      }

    case DeadStorage(op, deadDude, deadID, idList, globalMap) =>
      this.idList = idList
      this.globalMap = globalMap
      replicationSet.get(myBucket).head ! Op("new", deadDude, op, "", deadID, myID)

    case NewStorage(op, newDude, newID,idList, globalMap) =>
      this.idList = idList
      this.globalMap = globalMap
      replicationSet.get(myBucket).head ! Op("dead", newDude, op, "", newID, myID)

    case UpdateViews(idList, globalMap) =>
      this.idList = idList
      this.globalMap = globalMap


    case AddRepAck =>
      if(stopped){
        sender ! SendBucket(buckets.get(myBucket).head, myBucket)
      }

    case ExpandBucket(id)=>
      sender ! ExpandBucketAck(buckets.get(id).head, id)

    case ExpandBucketAck (bucket, id)=>
      var myBucketMap : Map[Int,String] = buckets(myBucket)
      buckets -= myBucket
      var newBucket = myBucketMap ++ bucket
      buckets += myBucket -> newBucket
      idList = idList.sorted
      var x = idList.indexOf(myBucket)
      var otherRep = globalMap.get(idList(x+2)).head
      context.actorSelection(otherRep) ! AddToReplication(myBucket, replicationSet.get(myBucket).head, id)
      context.actorSelection(globalMap.get(idList(x+1)).head) ! SendBucket(buckets.get(myBucket).head, myBucket)

    case SendBucket(bucket, bucketID)=>
      buckets += bucketID -> bucket
      sender ! SendBucketAck

    case SendBucketAck =>
      stopped = false
  }

  def NewDude(op : Int, newDude :String, newID : Int){
    /**
      * se op =  2 adicionar o novo gajo e quebrar ligaçao ao ultimo membro do replica set e copiar o bucket
      *
      *
      * se op = 1 dividir o novo bucket pelo id do novo membro dar lhe os ficherios daquele bcket adicionalo ao meu replica set e remover o ultimo do replica set
      *
      *
      *
      */
  }

  def DeadDude(op : Int, deadDude: String, deadID : Int){
    stopped = true
    if(op == 1){
      val deadDude = replicationSet.get(deadID).head
      idList = idList.sorted
      var x = idList.indexOf(myBucket)
      var otherRep = globalMap.get(idList(x+1)).head

      context.actorSelection(otherRep) ! ExpandBucket(deadID)
    }else{
      val deadDude = replicationSet.get(deadID).head
      idList = idList.sorted
      var x = idList.indexOf(myBucket)
      var otherRep = globalMap.get(idList(x+2)).head

      context.actorSelection(otherRep) ! AddToReplication(myBucket, replicationSet.get(myBucket).head, deadID)
    }
  }
}
