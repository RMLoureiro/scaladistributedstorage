import akka.actor._
import scala.collection.mutable.ListBuffer
import scala.collection.mutable.Buffer
import scala.collection.mutable.Set
case object Echo

case object Test

case object Alive


//triggers
case object PrintActiveView

case object SuffleTrigger

case class StartJoin(contact : String)

// HyParView Messages


//message sent to contact when joining overlay
case class JoinRequest(newID : String)

//random walker passed on to find random neighbours for new Node
case class ForwardJoin(lastSenderID : String, newID : String , timeToLive : Int)

//message sent to new Node by first neighbours
case class Neighbor(senderID : String, highPriority : Boolean)

//response to low priority neighbor request if active view is full
case class ActiveViewFull(senderID : String)


case class Disconnect(senderID : String)

case class ShuffleRequest(origin : String, lastSenderID : String, exchangeList : ListBuffer[String] ,timeToLive : Int)

case class ShuffleResponse(exchangeList : Buffer[String])

//message sent from gossip to HyParView requesting neighbours
case object ManosAtiva
//ManosAtiva response
case class CurrentNeighbors(actors: List[String])

//Message sent from globalView to HyParView signaling broadcast of a random dead dude
case class Obituary(deadID : String)


case class SendHeartBeat()
case class HeartBeat(heartbeat : Int, senderID: String)
case class DeadDude(actor: String)
case class NewDude(actor : String, id : Int)
case class CheckIfDudeDied(actor: String, senderID: String)
case class YouDead()
case class NotDead(myID: String)
case class NotDeadDude(dudeID: String)
case class HeartBeatResponse(str: String, i: Int)

case class RBroadcast(message: (String,String, Int))
case class GossipMessage(mid: String, message: (String,String, Int), origin: String)
case class AntiEntropy(messages : List[String])
case object AntiEntropyTrigger


case class StartGV(contact : String)
case class CurrentGV(contactGlobalView : Set[String]) 

case object AskGV 
case object PrintGV
case object StartUp
case object AddRepAck
case object SendBucketAck



case class Op (op : (String, String, Int, String, Int, String))
case class OrderedOp(serial : Int, op :(String, String, Int, String, Int, String))
case class Propose (serial : Int, op :(String, String, Int, String, Int, String), ref : String)
case class Accept (serial : Int, op : (String, String, Int, String, Int, String), ref : String)
case class NewMember(actor : ActorRef)
case class DeadMember(actor : ActorRef)
case class MPaxosConstructor(leader : String, serial: Int)
case class MPaxosStartup(ref: ActorRef)
case class TakeOver(op :(String, String, Int, String, Int,String))


case class GetNewId(newId :String)
case class GetId(id : Int)
case class ContinueJoin(newId: String, id: Int)
case class SendDeadDude(actor:String, senderID: String)

case class UpdateViews(idList: ListBuffer[Int], globalMap : Map[Int,String])
case class NewStorage(op: Int, newDude:String, newID : Int, idList: ListBuffer[Int], globalMap : Map[Int,String])
case class DeadStorage(op: Int, deadDude : String, deadID : Int, idList: ListBuffer[Int], globalMap : Map[Int,String])
case class StartUpStorage(message: String, x:Int ,y:Int)
case class Initialize(globalMap : Map[Int, String],  idList : ListBuffer[Int])
case class AddToReplication(newBucket: Int, pax : ActorRef,  oldBucket: Int)
case class AddPax(bucket : Int, myPax : ActorRef)
case class Read(clientID: String, id: Int)
case class Write(clientID: String, id: Int, content: String)
case class ReRead(buttersID : String, clientID: String, id: Int)
case class ReWrite(buttersID : String, clientID: String, id: Int, content: String, bucket: Int)
case class ReadResponse(clientID : String, id: Int, content: String)
case class ReadAck(id:Int , content:String)
case class AckWrite(id: Int, content:String)
case class SendBucket(bucket: Map[Int,String], bucketID : Int)
case class ExpandBucketAck (bucket:Map[Int,String], id:Int)
case class ExpandBucket(id : Int)