import akka.actor.{Cancellable, _}
import akka.util.Timeout

import scala.concurrent.ExecutionContext.Implicits.global
import scala.collection.mutable.ListBuffer
import scala.concurrent.duration._
import scala.util.Random


class Hyparview(myHostname : String ,myPort : String) extends Actor {

	val myID   = myHostname + ":" + myPort
	val gossip = "akka.tcp://ASD@"+myID+"/user/gossip"
	val globalID = "akka.tcp://ASD@"+myID+"/user/globalmembership"


	var activeView: Map[String, Int] = Map()
	var passiveView: Map[String, Int] = Map()
	var count : Int = 0
	var timeoutMessage : Map[String,(Cancellable,String)] = Map();
	val random = new Random

	//DEFAULT VALUES
	val TTL : Int = 3
	val ACTIVE_VIEW_MAX_SIZE : Int = 5
	val PASSIVE_VIEW_MAX_SIZE : Int = 12
	val KA = 2
	val KP = 3


	def receive = {
		////////////////////////////////////////////////
		//////////   Join Mechanism	    ////////////////
		////////////////////////////////////////////////

		case StartJoin(contact) =>
			getRefFromID(contact) ! JoinRequest(myID)
			//println(myPort +">> I sent JoinRequest to " + contact)


		case JoinRequest(newID) =>
			//println(myPort +">> I received JoinRequest from "+ newID);
			context.actorSelection(globalID) ! GetNewId(newID)

		case ContinueJoin(newID, id)=>
			for (activeString <- activeView.keySet)
				getRefFromID(activeString) ! ForwardJoin(myID, newID,TTL)

			addToActiveView(newID)

			getRefFromID(newID) ! Neighbor(myID, false)

			context.actorSelection(gossip) ! RBroadcast(("NewDude",newID, id))

			//println(myPort +">> I Neighbored "+ newID);


		case ForwardJoin(lastSenderID, newID, timeToLive) => 
			//println(myPort +">> I received ForwardJoin from "+ lastSenderID);
			if(timeToLive <= 0 && newID != myID) {
				addToActiveView(newID)

				if(activeView.size == 1){
					getRefFromID(newID) ! Neighbor(myID, true)
				}else{
					getRefFromID(newID) ! Neighbor(myID, false)
				}

			}else{

				if(activeView.size == 1 && newID != myID){
					getRefFromID(newID) ! Neighbor(myID, true)

					addToActiveView(newID)
				}
				else{

					val forwardTarget = getForwardCandidateDiff(lastSenderID)
					getRefFromID(forwardTarget) ! ForwardJoin(myID, newID, timeToLive -1)

					addToPassiveView(newID)
				}
				
			}



		case Neighbor(senderID, highPriority) =>

			//println(myID + ">> Got neighbor request from " + senderID)
			if(highPriority){
				//println(senderID +" needs a neighbor. adding...")
				addToActiveView(senderID)
			}else{
				//println(myPort +">> I was Neighbored by " + senderID)

				if(activeView.size == ACTIVE_VIEW_MAX_SIZE){
					getRefFromID(senderID) ! ActiveViewFull(myID)

					addToPassiveView(senderID)

					//println(myPort +">> Added " + senderID + " to Passive")
				}else{
					addToActiveView(senderID)
					//println(myPort +">> Added " + senderID + " to Active")
				}
			}

		case Disconnect(senderID) =>
			//println(myPort +">> Got Disconnect from " + senderID)
			if(activeView.size == 1){
				//println(myPort +">> sending urgent NeighborRequest to " + senderID)
				getRefFromID(senderID) ! Neighbor(myID, true)
			}
			else{
				//println(myPort +">> Droping " + senderID)
				activeView -= senderID
				addToPassiveView(senderID)
			}

		case ActiveViewFull(senderID) =>
			if(activeViewContains(senderID)){
				activeView -= senderID
			}

			addToPassiveView(senderID);


		////////////////////////////////////////////////
		//////////	   	Shuffles	 	////////////////
		////////////////////////////////////////////////

		case SuffleTrigger =>
			if(activeView.size > 0){
				
				val shuffleTarget = activeView.keySet.toVector(random.nextInt(activeView.size))
				var exchangeList = new ListBuffer[String]()
/*/
				println()
				println(myPort +">> Sending Suffle to " + shuffleTarget)
				println()

				println(myPort +">> ActiveView is ")
				for (s <- activeView) 
					println(s)

				println(myPort +">> passiveView is ")

				for (s <- passiveView) 
					println(s)
*/
				exchangeList ++= scala.util.Random.shuffle(activeView.keySet.toVector).take(KA)
			
				exchangeList ++= scala.util.Random.shuffle(passiveView.keySet.toVector).take(KP)

				exchangeList += myID
/*/
				println(myPort +">> ExchangeList is :")
				for (s <- passiveView) 
					println(s)
*/
				getRefFromID(shuffleTarget) ! ShuffleRequest(myID,myID, exchangeList, TTL)
/*/
				println()
				println(myPort +">> Suffle sent")
				println()
*/
			}

			


		case ShuffleRequest(origin,lastSenderId, exchangeList ,timeToLive) =>
			if(timeToLive <= 0 || activeView.size == 1) {



/*/
				println()
				println(myPort +">> Recieved shuffle request from " + origin)
				println()

				println(myPort +">> ActiveView is ")
				for (s <- activeView) 
					println(s)

				println(myPort +">> passiveView is ")

				for (s <- passiveView) 
					println(s)
				println(myPort +">> exchangeList is ")

				for (s <- exchangeList) 
					println(s)
*/
				
				var returnList = scala.util.Random.shuffle(passiveView.keySet.toVector).take(KA + KP).toBuffer

				
				
				var magicNumber = 0

				//if passive not full

				for(node <- exchangeList){
					if(node.equals(myID) || activeViewContains(node) || passiveViewContains(node)){
						exchangeList -= node
					}
				}

				//replenishing active View
				while((activeView.size < ACTIVE_VIEW_MAX_SIZE) && (exchangeList.size > 0) ){

					val nextNeighbour = exchangeList(random.nextInt(exchangeList.size))
					exchangeList -= nextNeighbour
					getRefFromID(nextNeighbour) ! Neighbor(myID, false)

					activeView = activeView + (nextNeighbour -> count)
				}


				var space = PASSIVE_VIEW_MAX_SIZE - passiveView.size

				while(space < exchangeList.size){
					passiveView = passiveView - returnList(random.nextInt(returnList.size))
					space = PASSIVE_VIEW_MAX_SIZE - passiveView.size
				}

				for(node <- exchangeList){
					passiveView = passiveView + (node -> count)
				}


				returnList += myID			
/*/
				println(myPort +">> new passiveView is ")
				for (s <- passiveView) 
					println(s)

				println(myPort +">> returnList is ")
				for (s <- returnList) 
					println(s)
*/

				getRefFromID(origin) ! ShuffleResponse(returnList)

/*/
				println()
				println(myPort +">> Shuffle resopnse sent to " + origin)
				println()
*/

			}
			else{

				val forwardTarget =	getForwardCandidateDiff(lastSenderId) 
/*/
				println()
				println(myPort +">> Forwarding suffle to " + forwardTarget)
				println()
*/
				getRefFromID(forwardTarget) ! ShuffleRequest(origin, myID, exchangeList, timeToLive -1)
			}

		case ShuffleResponse(exchangeList) =>

			for(node <- exchangeList){
				if(node.equals(myID) || activeViewContains(node) || passiveViewContains(node)){
					exchangeList -= node
				}
			}

			var space = PASSIVE_VIEW_MAX_SIZE - passiveView.size

			while(space < exchangeList.size){
				passiveView = passiveView - passiveView.keySet.toVector(random.nextInt(passiveView.size))
				space += 1
			}


			for(node <- exchangeList){
				passiveView = passiveView + (node -> count)

			}

		////////////////////////////////////////////////
		//////////  Failure Detection   ////////////////
		////////////////////////////////////////////////


		case SendHeartBeat =>
			if (activeView.nonEmpty){
				activeView.foreach { actor =>
					if(actor._2 < count - 5){
						println(myPort +">> I think " + actor._1 + " dieded")
						var globalList = passiveView.toList
						scala.util.Random.shuffle(globalList)
						getRefFromID(globalList.head._1) ! CheckIfDudeDied(actor._1, myID)

						//testing new Stuff
						//activeView = activeView - actor._1
						

					}else {
						getRefFromID(actor._1) ! HeartBeat(count, myID)
						println(myPort +">> Sending HB to " + actor)
					}
				}

				count +=1
			}

		case HeartBeat(heartbeat, senderID)=>
			if(activeView.contains(senderID)){
				if(heartbeat > activeView.get(senderID).head){
					activeView -= senderID
					activeView += senderID -> count
					getRefFromID(senderID) ! HeartBeatResponse(myID, heartbeat+1)
				}
			}

		case HeartBeatResponse(senderID, heartbeat) =>
			activeView -= senderID
			activeView += senderID -> heartbeat
			println(myPort +">> Recieved HB Response = " + senderID +" -> "+heartbeat)

		case DeadDude(actor) =>
			activeView -= actor
			context.actorSelection(gossip) ! RBroadcast(("DeadDude", actor, 0))

		case NewDude(actor, id) =>
			activeView += actor -> count

		case YouDead =>
			sender ! NotDead(myID)

		case NotDeadDude(dudeID) =>
			activeView -= dudeID
			activeView += dudeID -> count

		case CheckIfDudeDied(actor, senderID) =>
			val ref = getRefFromID(actor)
			val senderRef = getRefFromID(senderID)
			ref ! YouDead

			val system = context.system
			val timeout = Timeout(1 seconds)
			timeoutMessage +=  actor -> (system.scheduler.scheduleOnce(timeout.duration, self, SendDeadDude(actor, senderID)) -> senderID)


		case SendDeadDude(actor, senderID) =>
			getRefFromID(senderID) ! DeadDude(actor)


		case NotDead(senderID) =>
			if(timeoutMessage.contains(senderID)){
				timeoutMessage.get(senderID).head._1.cancel()
				getRefFromID(timeoutMessage.get(senderID).head._2) ! NotDeadDude(senderID)
				timeoutMessage -= senderID
			}


		case Obituary(deadID) =>
			activeView -= deadID
			passiveView -= deadID

	////////////////////////////////////////////////
	//////////	   Scala Stuff		////////////////
	////////////////////////////////////////////////


		case PrintActiveView => //printActiveView

		case ManosAtiva => 
			sender ! CurrentNeighbors(activeView.keySet.toList)


	} //end receive


	////////////////////////////////////////////////
	//////////	   Aux Functions	////////////////
	////////////////////////////////////////////////


	def addToActiveView(ID : String){
	
		if(!activeViewContains(ID) && (ID != myID) ){

			if(activeView.size >= ACTIVE_VIEW_MAX_SIZE){

				val chosen = activeView.keySet.toVector(random.nextInt(activeView.size))
				getRefFromID(chosen) ! Disconnect(myID)

				activeView = activeView - chosen

				addToPassiveView(chosen)
			}
			activeView = activeView + (ID -> count)
		}
		if(passiveViewContains(ID)){
			passiveView = passiveView - ID
				
		}
	}


	def addToPassiveView(ID : String){

		if(!activeViewContains(ID) && !passiveViewContains(ID) && (ID != myID) ) {

			if(passiveView.size >= PASSIVE_VIEW_MAX_SIZE ){
				passiveView = passiveView - passiveView.keySet.toVector(random.nextInt(passiveView.size))
			}
			passiveView = passiveView + (ID -> count)
		}
	}


	// returns a Neighbor different from lastSender
	def getForwardCandidateDiff(lastSender : String) : String ={
		val rdm = random.nextInt(activeView.size)
		var target = activeView.keySet.toVector(rdm)



		if(target.equals(lastSender)){
			target= activeView.keySet.toVector((rdm +1) % activeView.size)
		}
		target
	}


	def printActiveView(){
		println(myPort +">> TIMER")
		println(myPort +">> activeView.size = " + activeView.size)
		println(myPort +">> activeView = ")
		for (s <- activeView) 
			println("                     " + s._1)
		println(myPort +">> ")
		println(myPort +">> passiveView.size = " + passiveView.size)
		println(myPort +">> passiveView = ")
		for (s <- passiveView) 
			println("                     " + s._1)
		println()
	}

	def activeViewContains(s:String) : Boolean ={
		var contains = false
		for (activeString <- activeView.keySet) 
			if(activeString.equals(s)){
				contains = true
				//println(myPort +">> activeView contains " + s)
			}

		contains
	}
	def passiveViewContains(s:String) : Boolean ={
		var contains = false
		for (passiveString <- passiveView.keySet) 
			if(passiveString.equals(s)){
				contains = true
				//println(myPort +">> passiveView contains " + s)
			}

		contains
	}

	def getRefFromID(ID:String) : ActorSelection = {
		context.actorSelection("akka.tcp://ASD@"+ID+"/user/hyparview")
	}

	def getStringFromID(ID:String) : String = {
		"akka.tcp://ASD@"+ID+"/user/hyparview"
	}
 
}
