% !TEX TS-program = pdflatex
%
% Created by Joao Lourenco on 2017-10-06.
% Copyright (c) 2017 Joao Lourenco

\documentclass[11pt,a4paper]{article}

\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{hyperref}

\usepackage[english]{babel}
%\usepackage[portuguese]{babel}

\usepackage[margin=3cm]{geometry}
\usepackage{tabularx}
\usepackage{booktabs}
\usepackage{graphicx}
\usepackage{hyperref}

\newcommand{\mytitle}{ASD Project 1 Report}
\newcommand{\myauthor}{Ricardo Loureiro 45351 \& André Dias **** \& João Neves 45615}
\newcommand{\mydate}{\today}

\hypersetup
{
  pdftitle   = {\mytitle},
  pdfsubject = {Relatório do 1º Projeto da disciplina de Algoritmos de Sistemas Distribuidos},
  pdfauthor  = {\myauthor}
}

\title{\mytitle}
\author{\myauthor}
\date{\mydate}

\begin{document}

\maketitle


\begin{abstract}
%A very short summary of your problem, your solution and your results. 

This report accompanies our solution for a Global Membership System in a distributed P2P environment.

\end{abstract}

\begin{figure}[htbp]
  \centering
    \includegraphics[width=.4\textwidth]{logoUNL}
  \label{fig:logo_unl}
\end{figure}

\clearpage

\section{Introduction}
The goal of this phase of the Project was to implement a scalable global membership service that provides to each process a global view of all processes currently in the system. This membership service was built on top of a partial view membership service building an unstructured overlay network and using probabilistic reliable broadcast as an information dissemination strategy.
The user can interact with the system and terminate a process or ask for the global view of the strategy.


\section{Architecture}
Our solution has 4 layers :

\begin{itemize}
\item A Main scala entity that initializes the other 3 actors and serves as a command interpreter
\item A GlobalMembership layer, responsible for keeping an up-to-date list of all running processes
\item A Partial View layer that implements HyparView, responsible for maintaining an unstructured overlay network and detecting failed processes 
\item A Gossip layer that implements a Reliable Broadcast mechanism using lazy push and anti-entropy, responsible for spreading all messages to all correct processes
\end{itemize}



\begin{figure}[htbp]
  \centering
    \includegraphics[width=.75\textwidth]{arquitecture}
  \label{fig:arch}
\end{figure}


\section{Protocol Description}

The Gossip layer is a straightforward implementation of the first algorithm proposed to solve the reliable broadcast problem. The pseudo-code is in the slides for the third lecture.

The Hyparview layer has a lot of code (because of the failure detection and global membership support), but the underlying behavior is the same as described in the paper.
A node's ID is simply the port number scala/akka are binded to. The join mechanism suffered no changes. Same goes for the shuffle mechanism, with the exception of a small detail. When a node receives a shuffle response and needs to evict processes from it's passive view it does so at random, and not choosing the ID's sent in the original shuffle request. This does not impact too much the algorithm's efficiency, and was not implemented as described on the paper due to a lack of time.

The failure detection is made with a heartbeat protocol to all the neighbors in the active view and establishing a Time To Live of 5, after a process A fails to respond to the heartbeat request of process B five times in a row the process B starts to suspect that the process A died, after that it chooses a random process C from it's passive view and asks it to confirm that process A died by sending it a message. If process A responds process C informs process B that process A is alive else it responds saying its dead and process B eliminates process A from its active view and sends out a broadcast informing every process in the system that process A died.

The global membership is a very simple system that accepts a join from a new server, and sends a broadcast informing all the other processes of its existence. If HyParView detects a dead process it asks Gossip to broadcast the message. When a broadcast informing that a new process joined, it is added to the list of servers if that process is not in the tombstone set. If a process dies it's added to the tombstone set and removed from the global view. The tombstone set prevents that a delay in the messages cause a zombie process. This is, if a process joins and dies and the messages arrive in the wrong order informing that a process first died and then joined having a zombie, GlobalView might maintain a dead node indefinitely.

\section{Correctness}
Our objective for the Global Membership Service is for it to ensure that eventually, all processes that are correct and part of the system should be reported as part of the membership by every correct process, and that eventually, all failed processes are eventually not reported as part of the membership by every correct process.

Our Gossip layer ensures that all broadcasts are eventually received by every correct process. This means that if we broadcast every new join and every confirmed failure, eventually every global membership will report the same. Because our Gossip does not guarantee the messages are delivered in order, our Global membership layer keeps a tombstone for every process that is reported as failed, in the case of a process receiving the dead notice before the join notice (in which case the process would wrongly think the dead process just joined). This works but needs an ever-increasing list of dead nodes and makes it impossible for a dead node to be restarted with the same ID.

HyParView guarantees a connected graph and failure detection, probabilistic reliable broadcast guarantees all messages are delivered. Under this conditions, its easy to maintain an updated list of processes.

\section{Testing}

First things first, a lot of time was wasted codding mistakes in scala. The project is a lot smaller and easier if implemented in scala, but because everything is implicit, debugging becomes a nightmarish time waster very quickly in the beginning. And during most of the project. That being said:

To run and test the program we use SBT Assembly to generate a jar file, and we wrote a simple script that runs each process in its own port, with 1 second of interval between them. Each process is the contact of the next process to be created (if for any reason a node failed as it started, this would generate 2 disconnected graphs).

Because every node starts in a different terminal, it is easy to communicate with a specific process.

Because every node starts in a different terminal, it is hard to test with a lot of processes.

Because of this and because we didn't get the time to learn how to use the firewall to block arbitrary ports, no complex tests were conducted.
We settled on debugging with 7 nodes, and testing new features with 20. The global view is usually updated in less than a minute with the recently dead nodes, and quite fast with the new ones joining.

There are still a few cases we could not address in time.
This includes:
\begin{itemize}
\item In case of massive node failure, if the network is briefly broken in 2 or more subnetworks, the members of one will think the members of the other are dead and create the respective tombstones. Even when HyParView recovers network integrity the node's global views will eventually be empty.
\item We assume we can store messages indefinitely. This is of course, not scalable, but can be solved by remembering only the last X messages, X being a number that allows Gossip to disseminate a message to all nodes before that message is deleted.
\item Related to the last point, it is better if a joining node simply accepts it's contact's global view rather than constructing it from old broadcast messages.
\item Because of the tombstone approach, it is impossible for a process that fails to re-enter the system. The restarted node is accepted by the HyParView protocol, but will never be a part of any global view.
\end{itemize}

\section{Conclusions}


Coding this project taught us to appreciate the complexity of P2P networks and all the subtle details that need to be tested so a distributed system does not fail spectacularly when it is needed the most. We also learned not to underestimate the time it takes to learn a new language, even if it resembles one we are already familiar with.

In all fairness the project was fun to implement and see working, and most of all, was a good way to consolidate concepts from the course.


\end{document}
